#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Ant import Ant
from DataLoader import DataLoader
import random
import sys
import numpy as np
import matplotlib.pyplot as plt
import datetime

def main(alpha=1, beta=1):
    NUM_ANT = 1000
    NUM_CITY = 30

    dl = DataLoader()
    dl.load(count=NUM_CITY)

    ants = [Ant(dl.data, random.randint(0, NUM_CITY - 1), _alpha=alpha, _beta=beta) for i in range(NUM_ANT)]

    pheromone = np.array([[0 for i in range(len(dl.data))] for j in range(len(dl.data))])

    while True:
        delta = np.zeros_like(pheromone)

        for i in range(len(ants)):
            ant = ants[i]
            r = ant.move(pheromone)
            if r is None:
                break
            v = r["move"]
            p = r["new_pheromone"]

            delta[v[0]][v[1]] += p
            delta[v[1]][v[0]] += p

        if np.sum(delta) == 0:
            break

        pheromone *= 0.9
        pheromone += delta

    #結果
    #print(pheromone)

    """
    for i in range(len(pheromone)):
        for j in range(len(pheromone[i])):
            if j >= i:
                break

            xs = [dl.data[i].x, dl.data[j].x]
            ys = [dl.data[i].y, dl.data[j].y]

            if pheromone[i][j] != 0:
                plt.plot(xs, ys, 'k-', lw=np.log(pheromone[i][j]))
    """
    visited = [False for i in range(NUM_CITY)]
    route = []
    draw_best_route(dl.data, pheromone, visited, 0, route)

    for i in range(len(dl.data)):
        plt.text(dl.data[i][0], dl.data[i][1], i, color="red")
    plt.show()


def draw_best_route(points, pheromone, visited, i, route):
    a_pheromone = pheromone[i]
    not_visited = np.logical_not(visited)
    max_pheromone = np.max(a_pheromone[not_visited])
    indices = np.where(a_pheromone == max_pheromone)
    max_index = indices[0][0]

    visited[i] = True
    route.append(i)

    if np.sum(visited) == len(visited):
        #route.append(np.where(visited == False))
        print(route)
        np.savetxt("%s.txt" % datetime.datetime.now().strftime("%y%m%d%H%M%S"), route, fmt="%d")
        return

    xs = [points[i][0], points[max_index][0]]
    ys = [points[i][1], points[max_index][1]]

    plt.plot(xs, ys, 'k-')

    draw_best_route(points, pheromone, visited, max_index, route)



if __name__ == '__main__':
    sys.setrecursionlimit(1000000)

    alpha_and_beta = ((1, 1), (), (), (), (), ())

    for i in range(100):
        main()