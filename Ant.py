#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import numpy as np
from DataLoader import DataLoader


class Ant():
    def __init__(self, _atlas, _position, _alpha=1, _beta=1, _q=100):
        """
        :param _atlas: 座標情報を持ったクラスPointの配列
        :param _position: atlas中のどこにいるか，index
        :param _alpha: 計算式の次数
        :param _beta: 同上
        :param _q: フェロモンの量
        :return:
        """
        self.atlas = np.array(_atlas)
        self.position = _position
        visited = [False for i in range(len(self.atlas))]
        visited[_position] = True
        self.visited = visited
        self.alpha = _alpha
        self.beta = _beta
        self.q = _q

    def move(self, _pheromones):
        """
        :param _pheromones: フェロモンの情報，i->jはi,jとj,iに入れられている
        :return: None or 増分
        """

        f = False
        for b in self.visited:
            if b == False:
                f = True
        if f == False:
            print("ant move complete!")
            return None

        pheromones = np.array(_pheromones)

        a_pheromone = pheromones[self.position]
        a_distance = [((self.atlas[i][0] - self.atlas[self.position][0])**2 + (self.atlas[i][1] - self.atlas[self.position][1])**2)**(1/2)
                        for i in range(len(self.atlas))]

        weights = [a_pheromone[i]**self.alpha * a_distance[i]**(-self.beta) if self.visited[i] == False else 0
                        for i in range(len(self.atlas))]

        if np.sum(weights) == 0:
            weights = [1 if self.visited[i] == False else 0 for i in range(len(self.atlas))]

        #weightsの合計を1にする
        probabilities = weights / np.sum(weights)
        indices = np.array(range(len(self.atlas)))

        new_index = np.random.choice(indices, p=probabilities)

        previous_position = self.position
        self.position = new_index
        self.visited[new_index] = True

        new_pheromone = 100 / a_distance[new_index]
        move = (previous_position, self.position)

        return {"move": move, "new_pheromone": new_pheromone}


if __name__ == '__main__':
    dl = DataLoader()
    dl.load()

    position = random.randint(0,9)
    ant = Ant(dl.data, position)
    pheromone = [[0 for i in range(len(dl.data))] for j in range(len(dl.data))]

    print(ant.position)

    while True:
        r = ant.move(pheromone)
        if r is None:
            break

        v = r["move"]
        p = r["new_pheromone"]

        pheromone[v[0]][v[1]] += p
        pheromone[v[1]][v[0]] += p

        print(ant.position)