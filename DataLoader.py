#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import numpy as np
import matplotlib.pyplot as plt


class DataLoader():
    def __init__(self):
        self.data = None

    def load(self, count=10):
        #self.data = np.array([(random.random(), random.random()) for i in range(count)])
        self.data = np.loadtxt("data.txt")

if __name__ == '__main__':
    dl = DataLoader()
    dl.load(count=30)

    for i,v in enumerate(dl.data):
        plt.text(v[0], v[1], i)

    #np.savetxt("data.txt", dl.data)
    plt.show()